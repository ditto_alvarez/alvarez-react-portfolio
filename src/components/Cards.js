import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      <h1>Check out my projects!</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/card1.png'
              text='React js Portfolio'
              label='React JS'
              path='/services'
            />
            <CardItem
              src='images/img-2.jpg'
              text='Booking system'
              label='JavaScript'
              path='/services'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/img-6.jpg'
              text='HTML HTML HTML'
              label='HTML'
              path='/services'
            />
            <CardItem
              src='images/img-1.jpg'
              text='CSS CSS CSS'
              label='CSS'
              path='/products'
            />
            <CardItem
              src='images/img-7.jpg'
              text='MONGOBDBDBDBD'
              label='MongoDB'
              path='/sign-up'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
